<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'name' => 'Nguyen Van A',
            'email' => 'nva@gmail.com',
            'password' => Hash::make('123456'),
            'phonenumber' => '0123456789',
            'phonepassword' => Hash::make('123456'),
            'otp' => '',
            'cash' => '30000',
            'last_visited' => now(),
            'is_admin' => 1,
        ]);
    }
}
