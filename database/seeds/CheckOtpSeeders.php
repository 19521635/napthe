<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CheckOtpSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('check_otp')->truncate();
        DB::table('check_otp')->insert([
            'phonenumber' => '0123456789',
            'otp_password' => '123456'
        ]);
    }
}
