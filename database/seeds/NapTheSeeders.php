<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class NapTheSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('napthe')->truncate();
        DB::table('napthe')->insert([
            'email' => 'nva@gmail.com',
            'phonenumber' => '0123456789',
            'price' => '20000',
            'nhamang' => 'VTT',
            'loaitb' => 0,
            'status' => 1,
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
    }
}
