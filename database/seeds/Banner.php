<?php

use Illuminate\Database\Seeder;

class Banner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domain_name = parse_url(request()->root())['host'];
        DB::table('banner')->truncate();
        DB::table('banner')->insert([
            'content' => 'Xin chào quý khách đến với '.$domain_name,
        ]);
    }
}
