<?php

use Illuminate\Support\Facades\Route;
use App\Banner;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    $banner = new Banner();
    $model = $banner->get()->toArray();
    $domain_name = parse_url(request()->root())['host'];
    return view('index_napthe', ['domain_name'=>$domain_name,'txtcontent'=>$model[0]['content']]);
})->name('index_napthe');


Route::group(['middleware' => 'admin'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'AdminControllers@viewDashboard')->name('viewDashboard');
        Route::get('user', 'AdminControllers@listUser')->name('listUser');
        Route::get('card', 'AdminControllers@listCard')->name('listCard');
        Route::get('otp', 'AdminControllers@listOtp')->name('listOtp');

        Route::post('admin_ajax/getJsonUsers', 'AdminControllers@getJsonUsers')->name('getJsonUsers');
        Route::post('admin_ajax/updateUser', 'AdminControllers@updateUser')->name('updateUser');
        Route::post('admin_ajax/deleteUser', 'AdminControllers@deleteUser')->name('deleteUser');

        Route::post('admin_ajax/getJsonCards', 'AdminControllers@getJsonCards')->name('getJsonCards');
        Route::post('admin_ajax/updateCard', 'AdminControllers@updateCard')->name('updateCard');
        Route::post('admin_ajax/deleteCard', 'AdminControllers@deleteCard')->name('deleteCard');

        Route::post('admin_ajax/getJsonOtps', 'AdminControllers@getJsonOtps')->name('getJsonOtps');
        Route::post('admin_ajax/updateOtp', 'AdminControllers@updateOtp')->name('updateOtp');
        Route::post('admin_ajax/deleteOtp', 'AdminControllers@deleteOtp')->name('deleteOtp');

        Route::post('admin_ajax/getCashUser', 'AdminControllers@getCashUser')->name('getCashUser');
        Route::post('admin_ajax/updateCash', 'AdminControllers@updateCash')->name('updateCash');

        Route::post('update/banner', 'AdminControllers@banner');
    });
});

Route::post('user_ajax/login', 'UsersController@doLogin')->name('doLogin');
Route::get('user_ajax/logout', 'UsersController@doLogout')->name('doLogout');
Route::post('user_ajax/register', 'UsersController@doRegister')->name('doRegister');
Route::post('topup_ajax/topup', 'UsersController@doTopup')->middleware('auth')->name('doTopup');
Route::post('update/password','UsersController@password');
