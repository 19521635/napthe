<?php

namespace App\Http\Controllers;
use Auth;
use Redirect;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\Authorizes\Requests;
use Illuminate\Foundation\Auth\Access\Authorizes\Resources;
use Illuminate\Html\HtmlServiceProvider;
use App\User;
use App\NapThe;
use App\CheckOtp;
use Carbon\Carbon;
use Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use DB;


class UsersController extends Controller
{
    public function doLogout()
    {
        Auth::logout(); // logging out user
        return Redirect::route('index_napthe'); // redirection to login screen
    }
    public function doLogin(Request $request)
    {
        // Creating Rules for Email and Password
        $rules = array(
        'email' => 'required|email', // make sure the email is an actual email
        'password' => 'required|min:6'
        );
        // password has to be greater than 3 characters and can only be alphanumeric and);
        // checking all field
        $validator = Validator::make(Request::all() , $rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else
        {
            // create our user data for the authentication
            $userdata = array(
                'email' => Request::get('email') ,
                'password' => Request::get('password')
            );
            // attempt to do the login
            if (Auth::attempt($userdata))
            {
                $msg = array(
                    'status' => 'success',
                    'message' => 'Đăng nhập thành công!'
                );  
                User::where('id',Auth::id())->update(['last_visited'=>Carbon::now()]);
                return response()->json($msg, 200);
                // validation successful
                // do whatever you want on success
            }
            else
            {
                // validation not successful, send back to form
                $msg = array(
                    'status' => 'error',
                    'message' => 'Tài khoản hoặc mật khẩu không đúng!',
                    'errors' => array('errors'=>'Tài khoản hoặc mật khẩu không đúng!')
                );
                return response()->json($msg, 200);
            }
        }
    }

    // Register
    public function doRegister(Request $request) {
        $rules = array(
            'full_name' => 'required|max:255',
            'email' => 'required|email', // make sure the email is an actual email
            'password1' => 'min:6|required_with:password_confirmation',
            'password2' => 'min:6|same:password1',
            'phone' => 'min:10'
        );
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else
        {
            $users = new User;
            $users->name = Request::get('full_name');
            $users->email = Request::get('email');
            $users->password = hash::make(Request::get('password1'));
            $users->phonenumber = Request::get('phone');
            $users->created_at = Carbon::now();
            $users->updated_at = Carbon::now();
            $users->last_visited = Carbon::now();
            $users->cash = 0;
            $users->save();
            $msg = array(
                'status' => 'success',
                'message' => 'Add successful'
            );
            return response()->json($msg, 200);
        }
    }
    
    public function doTopup(Request $request) {
        $rules = array(
            'card' => 'required|min:3|max:3',
            'phone' => 'required|min:10|max:10', // make sure the email is an actual email
            'is_card' => 'integer|required',
            'topup_type' => 'required|max:1'
        );
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else
        {
            $user_otp = CheckOtp::firstOrCreate(
                    ['phonenumber'=>Request::get('phone')],
                    []
            );
            // switch case card
            $card_price = Request::get('is_card');
            switch ($card_price) {
                case 0:
                    $price = 50000;
                    break;
                case 1:
                    $price = 100000;
                    break;
                case 2:
                    $price = 150000;
                    break;
                case 3:
                    $price = 200000;
                    break;
                case 4:
                    $price = 300000;
                    break;
                case 5:
                    $price = 400000;
                    break;
                case 6:
                    $price = 500000;
                    break;
                default:
                    $msg = array(
                        'status' => 'error',
                        'message' => 'Nạp thẻ thất bại!',
                        'errors' => array('errors' => 'Nạp thẻ thất bại!')
                    );
                    return response()->json($msg, 200);
                    break;
                }
            // end
            $user_napthe = User::where('id',Auth::id())->first();

            if ($user_napthe->cash < $price) {
                $msg = array(
                    'status' => 'error',
                    'message' => 'Nạp thẻ thất bại!',
                    'errors' => array('errors' => 'Số tiền của bạn không đủ!')
                );
                return response()->json($msg, 200);
            } else {
                $napThe = new NapThe;
                $napThe->email = Auth::user()->email;
                $napThe->phonenumber = Request::get('phone');
                $napThe->price = $price;
                $napThe->nhamang = Request::get('card');
                $napThe->loaitb = Request::get('topup_type');
                $napThe->status = 0;
                $napThe->created_at = Carbon::now();
                $napThe->updated_at = Carbon::now();
                $napThe->save();

                
                $user_napthe->cash = $user_napthe->cash - $price;
                $user_napthe->save();
                Mail::to('Connectionstring@gmail.com')->send(new SendMail('Hệ thống thông báo tự dong napthegiare.com thông báo!',$napThe));
                $msg = array(
                    'status' => 'success',
                    'message' => 'Nạp thẻ thành công!'
                );
                return response()->json($msg, 200);
            }  
        }
    }
    public function password(Request $request){
        $userdata = array(
            'email' => Request::get('email') ,
            'password' => Request::get('password')
        );
        // attempt to do the login
        // \print_r($userdata).die;
        if (Auth::attempt($userdata) && Auth::check() === true){
            $where = array('email'=>Request::get('email'));
            $new_password = array('password'=>hash::make(Request::get('newpassword')));
            DB::table('users')->where($where)->update($new_password);
            Request::session()->flash('success', 'Thành Công');
            return \redirect('/');
        }else{
            Request::session()->flash('fail', 'thất bại');
            return \redirect('/');
        }
    }
}