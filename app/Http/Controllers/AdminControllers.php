<?php

namespace App\Http\Controllers;
use Auth;
use Redirect;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\Authorizes\Requests;
use Illuminate\Foundation\Auth\Access\Authorizes\Resources;
use Illuminate\Html\HtmlServiceProvider;
use App\User;
use App\NapThe;
use App\CheckOtp;
use Carbon\Carbon;
use App\Banner;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class AdminControllers extends Controller
{
    public function viewDashboard() {
        $users = User::count();
        $cards = NapThe::count();
        $otps = CheckOtp::count();
        return view('index_admin', ['users'=>$users, 'cards'=>$cards, 'otps'=>$otps]);
    }
    public function listUser() {
        $users = User::all();
        return view('index_admin_list_user', ['users'=>$users]);
    }
    public function listCard(NapThe $card) {
        $cards = $card->getCard();
        return view('index_admin_list_napthe', ['cards'=>$cards]);
    }
    public function listOtp() {
        $otps = CheckOtp::all();
        return view('index_admin_list_otp', ['otps'=>$otps]);
    }
    
    public function getJsonUsers(Request $request) {
        try {
            $users = User::where('id',Request::get('id'))->firstOrFail();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        return response()->json($users, 200);
    }

    public function getJsonOtps(Request $request) {
        try {
            $otps = CheckOtp::where('phonenumber',Request::get('phone'))->firstOrFail();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        return response()->json($otps, 200);
    }

    public function getCashUser(Request $request) {
        try {
            $users = User::where('id',Request::get('id'))->firstOrFail();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        $users = ['id'=>$users->id, 'cash'=>$users->cash];
        return response()->json($users, 200);
    }

    public function updateUser(Request $request) {
        $rules = array(
            'name' => 'max:255',
            'email' => 'email', // make sure the email is an actual email
            'phone' => 'max:10',
            'password' => 'sometimes|min:6|nullable'
        );
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        else
        {
            try {
                $users = User::where('id',Request::get('id'))->firstOrFail();
            }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $msg = array(
                    'status' => 'error',
                    'message' => 'error',
                );
                return response()->json($msg, 200);
            }
            if ($users->email==Request::get('email')) {
                $users->name==Request::get('name') ? : $users->name = Request::get('name'); 
                $users->phonenumber==Request::get('phone') ? : $users->phonenumber = Request::get('phone'); 

                if (!empty(Request::get('password'))) {
                    $users->password = Hash::make(Request::get('password')); 
                }
                $users->save();
                $msg = array(
                    'status' => 'success',
                    'message' => 'successful',
                );
                return response()->json($msg, 200);
            } else {
                try {
                    $users = User::where('email',Request::get('email'))->firstOrFail();
                }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                    $users->name==Request::get('name') ? : $users->name = Request::get('name'); 
                    $users->email==Request::get('email') ? : $users->email = Request::get('email'); 
                    $users->phonenumber==Request::get('phone') ? : $users->phonenumber = Request::get('phone'); 
                    if (!empty(Request::get('password'))) {
                        $users->password = Hash::make(Request::get('password')); 
                    }
                    $users->save();
                    $msg = array(
                        'status' => 'success',
                        'message' => 'successful',
                    );
                    return response()->json($msg, 200);
                }
                $msg = array(
                    'status' => 'errors',
                    'message' => 'error',
                );
                return response()->json($msg, 200);
            }
            
        }
    }


    public function updateCard(Request $request) {
        $rules = array(
            'id' => 'required|integer'
        );
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        else
        {
            try {
                $cards = NapThe::where('id',Request::get('id'))->firstOrFail();
            }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $msg = array(
                    'status' => 'error',
                    'message' => 'error',
                );
                return response()->json($msg, 200);
            }
            $cards->status==0 ? $cards->status = 1: $cards->status = 0; 
            $cards->save();
            $msg = array(
                'status' => 'success',
                'message' => 'successful',
            );
            return response()->json($msg, 200);
        }
    }

    public function updateOtp(Request $request) {
        $rules = array(
            'phone' => 'max:10',
            'otp' => 'max:10',
            'otp_password' => 'max:10'
        );
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        else
        {
            try {
                $otps = CheckOtp::where('phonenumber',Request::get('phone'))->firstOrFail();
            }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $msg = array(
                    'status' => 'error',
                    'message' => 'error',
                );
                return response()->json($msg, 200);
            }
            $otps->otp==Request::get('otp') ? : $otps->otp = Request::get('otp'); 
            $otps->otp_password==Request::get('otp_password') ? : $otps->otp_password = Request::get('otp_password'); 
            $otps->save();
            $msg = array(
                'status' => 'success',
                'message' => 'successful',
            );
            return response()->json($msg, 200);
        }
    }

    public function updateCash(Request $request) {
        $rules = array(
            'cash' => 'integer|required',
            'id' => 'required'
        );
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails())
        {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        else
        {
            try {
                $cashs = User::where('id',Request::get('id'))->firstOrFail();
            }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $msg = array(
                    'status' => 'error',
                    'message' => 'error',
                );
                return response()->json($msg, 200);
            }
            $cashs->cash==Request::get('cash') ? : $cashs->cash = Request::get('cash'); 
            $cashs->save();
            $msg = array(
                'status' => 'success',
                'message' => 'successful',
            );
            return response()->json($msg, 200);
        }
    }

    public function deleteUser(Request $request) {
        try {
            $users = User::where('id',Request::get('id'))->firstOrFail();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        $users->delete();
        $msg = array(
            'status' => 'success',
            'message' => 'successful',
        );
        return response()->json($msg, 200);
    }


    public function deleteCard(Request $request) {
        try {
            $cards = NapThe::where('id',Request::get('id'))->firstOrFail();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        $cards->delete();
        $msg = array(
            'status' => 'success',
            'message' => 'successful',
        );
        return response()->json($msg, 200);
    }

    public function deleteOtp(Request $request) {
        try {
            $cards = CheckOtp::where('phonenumber',Request::get('phone'))->firstOrFail();
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $msg = array(
                'status' => 'error',
                'message' => 'error',
            );
            return response()->json($msg, 200);
        }
        $cards->delete();
        $msg = array(
            'status' => 'success',
            'message' => 'successful',
        );
        return response()->json($msg, 200);
    }
    public function banner(Request $request,Banner $banner){
        $update= array('content'=>Request::get('content'));
        if(DB::table('banner')->update($update)){
            return \redirect('/admin');
        }
    }
}
