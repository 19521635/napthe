<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Database\QueryException;
use Throwable;
use Response;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof MethodNotAllowedHttpException) {
            return redirect('/');
        }
        if ($e instanceof TokenMismatchException){
            // Redirect to a form. Here is an example of how I handle mine
            return redirect($request->fullUrl())->with('csrf_error',"Oops! Seems you couldn't submit form for a long time. Please try again.");
        }

        if($e instanceof QueryException){
            $errorCode = $e->errorInfo[1];          
            switch ($errorCode) {
                case 1062://code dublicate entry 
                    $msg = array(
                        'status' => 'error',
                        'errors' => ['error'=>'Đã có tài khoản này trên hệ thống. ']
                    );
                    return response()->json($msg, 200);   
                    break;  
                case 7://code dublicate entry 
                    $msg = array(
                        'status' => 'error',
                        'errors' => ['error'=>'Đã có tài khoản này trên hệ thống. ']
                    );
                    return response()->json($msg, 200);   
                    break;  
            }
         }   

        return parent::render($request, $e);
        
    }
    
}
