<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class NapThe extends Model
{
    protected $table = "napthe";

    public $timestamps = true;

    public function getCard(){
        return DB::table('napthe')
        ->join('users','napthe.email','=','users.email')
        ->select('napthe.*','users.name as name')
        ->orderByRaw('napthe.id DESC')->get();
    }
    
}
