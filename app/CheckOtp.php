<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckOtp extends Model
{
    protected $table = "check_otp";

    protected $primaryKey = 'phonenumber';

    public $incrementing = false;

    protected $fillable = array('otp_password', 'phonenumber');

    public $timestamps = false;
}
