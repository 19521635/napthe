@extends('template.index_napthe')
@section('title', $domain_name.' - ĐỔI THẺ CÀO THÀNH TIỀN MẶT - GẠCH CƯỚC THUÊ BAO - UY TÍN NHANH CHÓNG')
@section('description', $domain_name.' là nơi thu mua thẻ cào uy tín, chiết khấu cao, có API kết nối tự động đến các website. Nơi gạch cước thuê bao trả trước, trả sau giá rẻ.')
@section('logo', $domain_name)

@section('row-content')
<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="panel panel-color panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">NẠP TRẢ TRƯỚC & TRẢ SAU</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-danger" role="alert" id="topup_err" style="display: none;"></div>
            <form method="post" action="{{Route('index_napthe')}}" id="topup_form1">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <select class="form-control" id="topup_telco" required>
                            <option value="">Chọn nhà mạng</option>
                            <option value="MBF">Mobifone</option>
                            <option value="VTT">Viettel</option>
                            <option value="VNF">Vina</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-8 col-xs-7 form-group" style="padding: 0;">
                        <input class="form-control" id="topup_phone" type="text"
                            placeholder="Số điện thoại cần nạp">
                    </div>
                    <div class="col-lg-4 col-xs-5 form-group" style="padding-left: 5px;">
                        <select class="form-control" id="topup_type" required>
                            <option value="">Loại thuê bao</option>
                            <option value="0">Trả trước</option>
                            <option value="1">Trả sau</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="is_big_card">
                            <option value="">Số tiền nạp</option>
                            <option value="0">Nạp thẻ 50k</option>
                            <option value="1">Nạp thẻ 100k</option>
                            <option value="2">Nạp thẻ 150k</option>
                            <option value="3">Nạp thẻ 200k</option>
                            <option value="4">Nạp thẻ 300k</option>
                            <option value="5">Nạp thẻ 400k</option>
                            <option value="6">Nạp thẻ 500k</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
            <button type='button' class="btn btn-block btn-success waves-effect w-md waves-light m-b-5"
                    id="topup_submit" value="Submit">NẠP TIỀN NGAY</button>
            </div>
        </div>
    </div>
    <div class="col-lg-2"></div>
</div>   
@endsection