@extends('template.index_napthe')
@section('title', $domain_name.' - ĐỔI THẺ CÀO THÀNH TIỀN MẶT - GẠCH CƯỚC THUÊ BAO - UY TÍN NHANH CHÓNG')
@section('description', $domain_name.' là nơi thu mua thẻ cào uy tín, chiết khấu cao, có API kết nối tự động đến các website. Nơi gạch cước thuê bao trả trước, trả sau giá rẻ.')
@section('logo', $domain_name)

@section('row-content')
<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="panel panel-color panel-primary">
            <div class="panel-heading">
            <h3 class="panel-title">NẠP TRẢ TRƯỚC & TRẢ SAU</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-warning" role="alert" id="topup_err" style="display: none;"></div>
                <form method="post" action="#" id="topup_form">
                    <div class="form-group">
                        <input class="form-control" style="display:none;" id="topup_pass" type="text"
                            placeholder="Mật khẩu (-3% nếu ko có)">
                    </div>
                    <div class="form-group">
                        <input class="form-control" id="topup_note" type="text"
                            placeholder="Vui lòng nhập mã OTP đã gửi qua số điện thoại">
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="button" class="btn btn-block btn-success waves-effect w-md waves-light m-b-5"
                    id="topup_submit">NẠP TIỀN NGAY</button>
            </div>
        </div>
    </div>
    <div class="col-lg-2"></div>
</div> 
@endsection