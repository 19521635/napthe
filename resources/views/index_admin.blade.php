@extends('template.index_admin')
@section('title', 'Admin Dashboard')
@section('description', 'Responsive Bootstrap 4 and web Application ui kit.')
@section('section_content')
<section class="content">
    <div class="">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Dashboard</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Aero</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                            class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                            class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong><i class="zmdi zmdi-chart"></i> WEB</strong> Report</h2>
                            <ul class="header-dropdown">
                                <li class="remove">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                        Quản cáo
                                      </button>
                                </li>
                            </ul>
                        </div>
                        <div class="body mb-2">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <div class="state_w1 mb-1 mt-1">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h5>{{$users}}</h5>
                                                <span><i class="zmdi zmdi-accounts"></i> Users</span>
                                            </div>
                                            <div class="sparkline" data-type="bar" data-width="97%" data-height="55px"
                                                data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#868e96">
                                                5,2,3,7,6,4,8,1</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <div class="state_w1 mb-1 mt-1">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h5>{{$cards}}</h5>
                                                <span><i class="zmdi zmdi-card"></i> Cards</span>
                                            </div>
                                            <div class="sparkline" data-type="bar" data-width="97%" data-height="55px"
                                                data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#2bcbba">
                                                8,2,6,5,1,4,4,3</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <div class="state_w1 mb-1 mt-1">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h5>{{$otps}}</h5>
                                                <span><i class="zmdi zmdi-code"></i> Otps</span>
                                            </div>
                                            <div class="sparkline" data-type="bar" data-width="97%" data-height="55px"
                                                data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#82c885">
                                                4,4,3,9,2,1,5,7</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <div class="state_w1 mb-1 mt-1">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h5>......</h5>
                                                <span><i class="zmdi zmdi-more"></i> ......</span>
                                            </div>
                                            <div class="sparkline" data-type="bar" data-width="97%" data-height="55px"
                                                data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#45aaf2">
                                                7,5,3,8,4,6,2,9</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div id="chart-area-spline-sracked" class="c3_chart d_sales"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form action="/admin/update/banner" method="POST">
@csrf
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="md-form mb-3">
                    <label data-error="wrong" data-success="right" for="name" style="font-weight:bold;">Quảng cáo banner:</label>
                    <input type="text" name="content" class="form-control validate">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection