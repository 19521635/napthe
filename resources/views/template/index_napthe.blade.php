
<!DOCTYPE html>
<html lang="vi">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

@include('master.index_napthe_head')

<body>
    <!-- Navigation Bar-->
    <div class="loading">
        <div class="lds-hourglass"></div>
    </div>
    @include('master.index_napthe_headnav')
    <div class="wrapper">
        <div class="container">
            <!-- Page-Title -->
            @include('master.index_napthe_top_menu')
            <!-- end page title end breadcrumb -->
            @include('master.index_napthe_animation_rtl')

            @yield('row-content')

        </div>
    </div>
    <!-- Modal -->
    @include('master.index_napthe_login_modal')
    @include('master.index_napthe_register_modal')
    @include('master.index_napthe_otp_modal')
    <!-- End Modal -->
    @include('master.index_napthe_script')
</body>

</html>