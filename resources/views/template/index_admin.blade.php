
<!doctype html>
<html class="no-js " lang="en">


@include('master.index_admin_head')

<body class="theme-blush">

@include('master.index_admin_page_loader')

@include('master.index_admin_search')

@include('master.index_admin_right_sidebar')


@include('master.index_admin_left_sidebar')

@include('master.index_admin_config_right_sidebar')

<!-- Main Content -->

@yield('section_content')

{{-- end main content --}}

@include('master.index_admin_script')
</body>


</html>