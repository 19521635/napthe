<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#dcf0f8"
    style="margin:0;padding:0;background-color:#f2f2f2;width:100%!important;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">
    <tbody>
        <tr>
            <td align="center" valign="top"
                style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">
                <table border="0" cellpadding="0" cellspacing="0" width="600" style="margin-top:15px">
                    <tbody>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="line-height:0">
                                    <tbody>

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background:#fff">
                            <td align="left" width="600" height="auto" style="padding:15px">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h1
                                                    style="font-size:14px;font-weight:bold;color:#444;padding:0 0 5px 0;margin:0">
                                                    Hệ thống gửi Napthegiare.com thông báo có đơn hàng!
                                                    !</h1>
                                                <h3
                                                    style="font-size:13px;font-weight:bold;color:#e47297;text-transform:uppercase;margin:20px 0 0 0;border-bottom:1px solid #ddd">
                                                    Thông tin đơn hàng <span
                                                        style="font-size:12px;color:#777;text-transform:none;font-weight:normal">(Ngày
                                                        <?php echo date("d");?> tháng <?php echo date("m");?> năm <?php echo date("Y");?> - <?php echo date("h:i:sa");?>)</span> </h3>
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>
                                        <tr>
                                            <td
                                                style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">

                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th align="left" width="30%"
                                                                style="padding:6px 9px 0px 9px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;font-weight:bold">
                                                                Họ và tên</th>
                                                            <th align="left" width="30%"
                                                                style="padding:6px 9px 0px 9px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;font-weight:bold">
                                                                Số điện thoại</th>
                                                            <th align="left" width="50%"
                                                                style="padding:6px 9px 0px 9px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;font-weight:bold">
                                                                Email</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top"
                                                                style="padding:3px 9px 9px 9px;border-top:0;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">
                                                            <span style="text-transform:capitalize">{{Auth::user()->name}}</span><br>
                                                            </td>
                                                            <td valign="top"
                                                                style="padding:3px 9px 9px 9px;border-top:0;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">
                                                                <span style="text-transform:capitalize">{{$e_message->phonenumber}}</span><br>
                                                            </td>
                                                            <td valign="top" style="padding:3px 9px 9px 9px;border-top:0;border-left:0;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">
                                                                <a href="mailto:{{$e_message->email}}"
                                                                target="_blank">{{$e_message->email}}</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h2
                                                    style="text-align:left;margin:10px 0;border-bottom:1px solid #ddd;padding-bottom:5px;font-size:13px;color:#e47297">
                                                    CHI TIẾT ĐƠN HÀNG</h2>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                                    style="background:#f5f5f5">
                                                    <thead>
                                                        <tr>
                                                            <th align="left" bgcolor="#e47297" width="30%"
                                                                style="padding:6px 9px;color:#fff;text-transform:uppercase;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px;text-align: center;">
                                                                Nhà mạng</th>
                                                            <th align="left" bgcolor="#e47297" width="40%"
                                                                style="padding:6px 9px;color:#fff;text-transform:uppercase;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px;text-align: center;">
                                                                Mệnh giá</th>
                                                            <th align="left" bgcolor="#e47297" width="30%"
                                                                style="padding:6px 9px;color:#fff;text-transform:uppercase;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px;text-align: center;">
                                                                Loại thuê bao</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody bgcolor="#eee"
                                                        style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">
                                                        <tr>
                                                            <td align="left" valign="top" style="padding:3px 9px;text-align: center;">
                                                                <strong>-----------------------------------------@if($e_message->nhamang=="MBF") Mobiphone @elseif($e_message->nhamang=="VTT") Viettel @else Vinaphone @endif-----------------------</strong>
                                                            </td>

                                                            <td align="left" valign="top" style="padding:3px 9px;text-align: center;">
                                                            <span><strong>  {{number_format($e_message->price)}}&nbsp;₫</strong></span></td>
                                                            <td align="left" valign="top" style="padding:3px 9px;text-align: center;"><strong>@if($e_message->loaitb==0) Trả trước @else Trả sau @endif</strong></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>