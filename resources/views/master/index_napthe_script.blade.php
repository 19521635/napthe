<script>
    var fb_app_id = '392124458002962';
    var base_url = '';
</script>
<script src="{{asset('statics/js/jquery.min.js')}}"></script>
<script src="{{asset('statics/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('statics/js/bootstrap.min.js')}}"></script>
<script src="{{asset('statics/js/functionsdeda.js?v=70')}}"></script>
<script src="{{asset('statics/js/loginfb1f2b.js?v=1584414549')}}"></script>
<script src="{{asset('statics/js/plugins/counterup/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('statics/js/plugins/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('statics/js/jquery.core.js')}}"></script>
<script src="{{asset('statics/js/plugins/sweet/sweet-alert.min.js')}}"></script>
<script src="{{asset('statics/js/homef3ee.js?v=3.2')}}"></script>
<span class="sa-totop"></span>
<script>
    $(function () {
        $('.navigation-menu li.has-submenu a[href="#"]').on('click', function (e) {
            if ($(window).width() < 992) {
                e.preventDefault();
                $(this).parent('li').toggleClass('open');
            }
        });
        $('#btn_login').click(function () {
            accountLogin();
        });
        $('#btn_register').click(function () {
            accountRegister();
        });
        $('#btn_forgot').click(function () {
            forgotPass();
        });
        $('#login_email, #login_pass').keydown(function (e) {
            if (e.keyCode == 13) {
                accountLogin();
            }
        });
        if (getCookie('popup_notify') == '') {
            $('#show_popup_notify').click();
            setCookie('popup_notify', 'yes', 30);
        }
    });
</script>

<script>
    $(document).ready(function () {
        $('#topup_telco').change(function (e) { 
            e.preventDefault();
            $('#topup_pass').show();
        });
    });
</script>
<script src="{{asset('statics/js/plugins/check.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
       @if(Session:: has('fail'))
            swal("THẤT BẠI", "Mật khẩu củ không đúng", "error");
       @endif
       @if(Session:: has('success'))
            swal("THÀNH CÔNG", "Đổi mật khẩu thành công", "success");
       @endif
    });
</script>