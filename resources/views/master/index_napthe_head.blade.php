<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="description"
        content="@yield('description')">
    <meta name="keywords" content="{{$domain_name}}, gach the, doi the cao thanh tien mat, gach the cham, gach the nhanh">
    <link rel="shortcut icon" href="{{asset('statics/images/favicon.ico')}}">

    <meta property="og:url" content="dang-ky-tai-khoan.html" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description"
        content="@yield('description')" />
    <meta property="og:image" content="{{asset('statics/images/ts.png')}}" />
    <meta property="fb:app_id" content="392124458002962" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">



    <!-- App css -->
    <link href="{{asset('statics/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/core.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/components.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/icons.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/pages.css')}}" rel="stylesheet">
    <link href="{{asset('statics/css/menu3f71.css?v=1.1.1')}}" rel="stylesheet">
    <link href="{{asset('statics/css/responsive.css')}}" rel="stylesheet">
    <style>
        .navbar-c-items button {
            margin-top: 12px;
            margin-left: 5px;
        }

        .navbar-c-items span {
            font-size: 16px;
            margin: 12px 5px;
            float: left;
            display: block;
            padding: .6em;
        }

        .dropdown-money {
            padding: 0px;
        }

        .dropdown-money li {
            padding: 4px 6px;
            color: #fff;
            font-size: 16px;
            width: 200px;
        }

        .money-mobile {
            display: none !important;
        }

        .money-mobile button {
            margin-top: 12px;
            padding: 5px 10px;
            font-size: 14px;
        }

        @media (max-width: 720px) {
            .money-mobile {
                display: block !important;
            }

            .money-pc {
                display: none !important;
            }

            .chat_live {
                display: none !important;
            }
        }

        .loading {
            background-color: #000000c4;
            top: 0;
            left: 0;
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 9998;
            display: none;
        }

        .lds-hourglass {
            display: inline-block;
            position: fixed;
            width: 64px;
            height: 64px;
            z-index: 9999;
            top: 45%;
            left: calc(50% - 32px);
        }

        .lds-hourglass:after {
            content: " ";
            display: block;
            border-radius: 50%;
            width: 0;
            height: 0;
            margin: 6px;
            box-sizing: border-box;
            border: 26px solid #fff;
            border-color: #fff transparent #fff transparent;
            animation: lds-hourglass 1.2s infinite;
        }

        @keyframes lds-hourglass {
            0% {
                transform: rotate(0);
                animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
            }

            50% {
                transform: rotate(900deg);
                animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
            }

            100% {
                transform: rotate(1800deg);
            }
        }
    </style>
</head>