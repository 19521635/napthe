<div id="login-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="login_form">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">ĐĂNG NHẬP TÀI KHOẢN</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="alert alert-danger" role="alert" id="login_err" style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Địa chỉ Email</label>
                                    <input type="email" class="form-control" id="login_email"
                                        placeholder="Nhập địa chỉ Email" autofocus="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Mật khẩu</label>
                                    <input type="password" class="form-control" id="login_pass"
                                        placeholder="Nhập mật khẩu">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 col-xs-6" style="padding-left:  0px;">
                                <div class="form-group">
                                    <a href="#" data-dismiss="modal" data-toggle="modal"
                                        data-target="#forgot-modal">Đổi mật khẩu?</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 pull-right">
                                <div class="form-group pull-right">
                                    <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#register-modal">Đăng ký tài khoản mới!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info waves-effect waves-light" id="btn_login">ĐĂNG
                            NHẬP</button>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">HỦY</button>
                    </div>
                </form>
            </div>
        </div>
    </div>