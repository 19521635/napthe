<header id="topnav">
    <div class="topbar-main">
        <div class="container">
            <!-- Logo container-->
            <div class="logo">
                <p>@yield('logo')</p>
            </div>
            <!-- End Logo container-->

            <div class="menu-extras">
                <ul class="nav navbar-nav navbar-right pull-right">
                    @if(Auth::check())
                    @if(Auth::user()->is_admin==0)
                    <li class="navbar-c-items">
                        <button type="button" class="btn btn-default btn-bordered waves-effect w-md m-b-5">Cash: {{number_format(Auth::user()->cash)}} vnđ</button>
                    </li>
                    @endif
                    <li class="navbar-c-items">
                        <div class="dropdown clearfix">
                            <button class="btn btn-teal btn-bordered waves-light waves-effect w-md m-b-5 dropdown-toggle" type="button" id="dropdownMenuDivider" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="font-weight:bold;">
                              {{Auth::user()->name}}
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuDivider">
                                @if(Auth::user()->is_admin==1)
                                <li><a href="{{Route('viewDashboard')}}">Quản lý Admin</a></li>
                                @endif
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ Route('doLogout') }}">Log out</a></li>
                            </ul>
                          </div>
                    </li>
                    
                    @else
                    <li class="navbar-c-items">
                        <button type="button" class="btn btn-teal btn-bordered waves-light waves-effect w-md m-b-5"
                            data-toggle="modal" data-target="#login-modal">ĐĂNG NHẬP</button>
                    </li>
                    <li class="navbar-c-items">
                        <button type="button"  data-toggle="modal" data-target="#register-modal"
                            class="btn btn-default btn-bordered waves-effect w-md m-b-5">ĐĂNG KÝ</button>
                    </li>
                    @endif
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>
        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->
</header>