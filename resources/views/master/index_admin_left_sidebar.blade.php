<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{Route('viewDashboard')}}"><img src="{{asset('assets/images/logo.svg')}}" width="25" alt="Aero"><span
                class="m-l-10">Aero</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <a class="image" href="#"><img src="{{asset('assets/images/profile_av.jpg')}}" alt="User"></a>
                    <div class="detail">
                        <h4>ADMIN</h4>
                        <small>Super Admin</small>
                    </div>
                </div>
            </li>
            <li class="@if(Route::currentRouteName() == 'viewDashboard') active @endif open"><a
                    href="{{Route('viewDashboard')}}"><i class="zmdi zmdi-home"></i><span>Trang quản lý</span></a></li>
            <li class="@if(Route::currentRouteName() == 'listUser') active @endif open"><a
                    href="{{Route('listUser')}}"><i class="zmdi zmdi-accounts"></i><span>Users</span></a>
            </li>
            <li class="@if(Route::currentRouteName() == 'listCard') active @endif open"><a
                    href="{{Route('listCard')}}"><i class="zmdi zmdi-card"></i><span>Cards</span></a>
            </li>
        </ul>
    </div>
</aside>