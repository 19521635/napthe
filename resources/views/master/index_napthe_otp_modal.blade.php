<div id="otp-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="#" id="topup_form">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">NẠP TRẢ TRƯỚC & TRẢ SAU</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="alert alert-warning" role="alert" id="otp_err" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="otp_phonenumber" type="tel" style="display: none;">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" id="otp_code" type="text"
                                        placeholder="Vui lòng nhập mã OTP đã gửi qua số điện thoại">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-block btn-success waves-effect w-md waves-light m-b-5"
                                    id="otp_submit">NẠP TIỀN NGAY</button>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">HỦY</button>
                    </div>
                </form>
            </div>
        </div>
    </div>