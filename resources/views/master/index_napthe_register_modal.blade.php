<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <form id="register_form">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">ĐĂNG KÝ TÀI KHOẢN</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="alert alert-danger" role="alert" id="register_err" style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="full_name">Họ và tên<span class="text-danger">*</span></label>
                            <input type="text" name="full_name" parsley-trigger="change" placeholder="Nhập tên đầy đủ"
                                class="form-control" id="full_name" required>
                        </div>
                        <div class="form-group">
                            <label for="emailAddress">Địa chỉ Email (Gmail)<span class="text-danger">*</span></label>
                            <input type="email" name="email" parsley-trigger="change" required
                                placeholder="Chỉ chấp nhận Gmail" class="form-control" id="emailAddress">
                        </div>
                        <div class="form-group">
                            <label for="phone">Số điện thoại<span class="text-danger">*</span></label>
                            <input type="text" name="phone" data-parsley-type="number" required
                                placeholder="Nhập số di động" class="form-control" id="phone">
                        </div>
                        <div class="form-group">
                            <label for="pass1">Mật khẩu<span class="text-danger">*</span></label>
                            <input name="pass" id="pass1" type="password" placeholder="Mật khẩu" required
                                class="form-control" data-parsley-minlength="6">
                        </div>
                        <div class="form-group">
                            <label for="passWord2">Xác nhận mật khẩu <span class="text-danger">*</span></label>
                            <input data-parsley-equalto="#pass1" type="password" required placeholder="Nhập lại mật khẩu"
                                class="form-control" id="passWord2">
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LdUaXYUAAAAAOZP4_vtT7l7NasPOTikE1goQ-nL"></div>
                        </div>
                    </div>  
                    <div class="form-group text-right m-b-0 modal-footer">
                        <button type="button" class="btn btn-primary waves-effect waves-light" id="btn_register">
                            ĐĂNG KÝ NGAY
                        </button>
                        <button type="button" class="btn btn-default waves-effect m-l-5" data-dismiss="modal">
                            HỦY BỎ
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- reset password --}}
    <div id="forgot-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/update/password" method="POST">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">ĐỔI MẬT KHẨU</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="alert alert-success" role="alert" id="forgot_success"
                                        style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="alert alert-danger" role="alert" id="forgot_err" style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Địa chỉ Email</label>
                                    <input type="email" class="form-control" name="email"
                                        placeholder="Nhập địa chỉ Email" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Mật khẩu cũ</label>
                                    <input type="password" class="form-control" name="password"
                                        placeholder="Nhập mật khẩu cũ" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Mật khẩu mới</label>
                                    <input type="password" class="form-control" name="newpassword"
                                        placeholder="Nhập mật mới" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light" id="btn_forgot">ĐỔI MẬT KHẨU
                            KHẨU</button>
                        <button type="reset" class="btn btn-default waves-effect" data-dismiss="modal">HỦY</button>
                    </div>
                </form>
            </div>
        </div>
    </div>