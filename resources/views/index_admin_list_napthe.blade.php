@extends('template.index_admin')
@section('title', 'Admin Dashboard')
@section('description', 'Responsive Bootstrap 4 and web Application ui kit.')
@section('section_content')
<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Cards</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/user"><i class="zmdi zmdi-home"></i> User</a></li>
                        <li class="breadcrumb-item"><a href="/admin/otp">OTP</a></li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">                
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>                                
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Thông tin đơn hàng</strong></h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable autotable">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>SĐT</th>
                                            <th>Mệnh giá</th>
                                            <th>Nhà mạng</th>
                                            <th>Loại</th>
                                            <th>Trạng thái</th>
                                            <th>Ngày</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($cards as $card)
                                    <tr>
                                        <td></td>
                                        <td>{{$card->name}}</td>
                                        <td>{{$card->email}}</td>
                                        <td>{{$card->phonenumber}}</td>
                                        <td>{{number_format($card->price)}} đ</td>
                                        <td>@if($card->nhamang=="MBF") Mobiphone @elseif($card->nhamang=="VTT") Viettel @else Vinaphone @endif</td>
                                        <td>@if($card->loaitb==0) Trả trước @else Trả sau @endif</td>
                                        @if($card->status==1)<td style="font-weight:bold; color:green";"> Đã nạp @else<td style="font-weight:bold; color:red;"> Chưa nạp @endif</td>
                                        <td>{{$card->created_at}}</td>

                                    <td>@if($card->status == 0)<button type="button" style="background:#1bccab" class="edit-card btn btn-outline-secondary btn-sm" value="{{$card->id}}" ><i class="zmdi zmdi-edit"></i> Duyệt</button>@endif</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection