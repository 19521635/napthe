@extends('template.index_admin')
@section('title', 'Admin Dashboard')
@section('description', 'Responsive Bootstrap 4 and web Application ui kit.')
@section('section_content')
<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Cards</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/user"><i class="zmdi zmdi-home"></i> Users</a></li>
                        <li class="breadcrumb-item"><a href="/admin/otp">OTP</a></li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">                
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>                                
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Mật khẩu OTP</strong></h2>
                            {{-- <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul> --}}
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable autotable">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>SĐT</th>
                                            <th>Mật khẩu OTP</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($otps as $otp)
                                    <tr>
                                        <td></td>
                                        <td>{{$otp->phonenumber}}</td>
                                        <td>{{$otp->otp_password}}</td>
                                    <td style="text-align: center">
                                        <button type="button" style="background:#34c738" class="edit-otp btn btn-outline-secondary btn-sm" value="{{$otp->phonenumber}}" data-toggle="modal" data-target="#elegantModalForm"><i class="zmdi zmdi-edit"></i> Cập nhật</button>
                                        <button type="button" style="background:red" class="delete-otp btn btn-outline-secondary btn-sm" value="{{$otp->phonenumber}}">   <i class="zmdi zmdi-delete"></i> Xóa</button></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="elegantModalForm" class="elegantModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content form-elegant">
      <!--Header-->
      <div class="modal-header text-center">
        <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Cập nhật mật khẩu</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body mx-6">
        <!--Body-->
        <div class="md-form mb-3" style="display: none;">
            <label data-error="wrong" data-success="right" for="phone" style="font-weight:bold;">ID</label>
            <input type="number" id="phone" class="form-control validate">
        </div>
        {{-- <div class="md-form mb-3">
            <label data-error="wrong" data-success="right" for="otp" style="font-weight:bold;">OTP:</label>
            <input type="text" id="otp" class="form-control validate">
        </div> --}}
        <div class="md-form mb-3">
            <label data-error="wrong" data-success="right" for="otp_password" style="font-weight:bold;">OTP PASSWORD:</label>
            <input type="text" id="otp_password" class="form-control validate">
        </div>

        <div class="text-center mb-3">
          <button type="button" class="btn blue-gradient btn-block btn-rounded z-depth-1a" id="updateOtp">Cập Nhật</button>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!-- Modal -->
@endsection