@extends('template.index_admin')
@section('title', 'Admin Dashboard')
@section('description', 'Responsive Bootstrap 4 and web Application ui kit.')
@section('section_content')
<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Users</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/card"><i class="zmdi zmdi-home"></i> Cards</a></li>
                        <li class="breadcrumb-item"><a href="/admin/otp">OTP</a></li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">                
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>                                
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Quản lý User</strong></h2>
                            {{-- <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul> --}}
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable autotable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên</th>
                                            <th>Email</th>
                                            <th>SĐT</th>
                                            <th>Tài sản</th>
                                            <th>Hoạt động lần cuối</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                        <td></td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phonenumber}}</td>
                                        <td>{{number_format($user->cash)}} VND</td>
                                        <td>{{$user->last_visited}}</td>
                                        <td>
                                            <button type="button" class="edit-user btn btn-outline-secondary btn-sm" style="background:#1bccab" value="{{$user->id}}" data-toggle="modal" data-target="#elegantModalForm"><i class="zmdi zmdi-edit"></i></button> 
                                            <button type="button" class="cash-user btn btn-outline-secondary btn-sm" style="background:#36c536" value="{{$user->id}}">   <i class="zmdi zmdi-money"></i></button>
                                            <button type="button" class="delete-user btn btn-outline-secondary btn-sm" style="background:red" value="{{$user->id}}">   <i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="elegantModalForm" class="elegantModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content form-elegant">
      <!--Header-->
      <div class="modal-header text-center">
        <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Cập nhật</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body mx-6">
        <!--Body-->
        <div class="md-form mb-3" style="display: none;">
            <label data-error="wrong" data-success="right" for="id" style="font-weight:bold;">ID</label>
            <input type="number" id="id" class="form-control validate">
        </div>
        {{-- <div class="md-form mb-3">
            <label data-error="wrong" data-success="right" for="role" style="font-weight:bold;">Role:</label>
            <select class="form-control show-tick ms select2" id="role" name="role" data-placeholder="Select">
                <option value="0" id="role-user">User</option>
                <option value="1" id="role-admin">Admin</option>
            </select>
        </div> --}}
        <div class="md-form mb-3">
            <label data-error="wrong" data-success="right" for="name" style="font-weight:bold;">Tên:</label>
            <input type="text" id="name" class="form-control validate">
        </div>
        <div class="md-form mb-3">
            <label data-error="wrong" data-success="right" for="email" style="font-weight:bold;">Email:</label>
            <input type="email" id="email" class="form-control validate">
        </div>
        <div class="md-form pb-3">
            <label data-error="wrong" data-success="right" for="phone" style="font-weight:bold;">Số điện thoại:</label>
            <input type="tel" id="phone" class="form-control validate">
        </div>
        <div class="md-form pb-3">
            <label data-error="wrong" data-success="right" for="password" style="font-weight:bold;">Mật khẩu:</label>
            <input type="text" id="password" class="form-control validate">
        </div>

        <div class="text-center mb-3">
          <button type="button" class="btn blue-gradient btn-block btn-rounded z-depth-1a" id="updateUser">Cập Nhật</button>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="elegantModalForm1" class="elegantModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content form-elegant">
      <!--Header-->
      <div class="modal-header text-center">
        <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Nạp tiền tài khoản</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body mx-6">
        <!--Body-->
        <div class="md-form mb-3" style="display: none;">
            <label data-error="wrong" data-success="right" for="id" style="font-weight:bold;">ID</label>
            <input type="number" id="id" class="form-control validate">
        </div>
        
        <div class="md-form pb-3">
            <label data-error="wrong" data-success="right" for="cash" style="font-weight:bold;">Số tiền:</label>
            <input type="number" id="cash" class="form-control validate">
        </div>

        <div class="text-center mb-3">
          <button type="button" class="btn blue-gradient btn-block btn-rounded z-depth-1a" id="updateCash">Cập nhật</button>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!-- Modal -->
  
@endsection