$(function() {

	$('#sms_game_form input').on('keypress', function (event) {
		if (event.which === 13) {
			$('#sms_game_submit').click();
		}
	});

	$('#sms_game_submit').click(function () {
		var syntax = $('#sms_game_syntax');
		var quantity = $('#sms_game_quantity');
		var note = $('#sms_game_note');

		hideErrorForm($('#sms_game_form'), $('#sms_game_err'));

		if(syntax.val() == '') {
			showErrorForm($('#sms_game_err'), syntax, 'Bạn cần nhập cú pháp SMS!');
			return;
		}
		if(quantity.val() == '' || parseInt(quantity.val()) < 1) {
			showErrorForm($('#sms_game_err'), quantity, 'Bạn cần nhập số lần gửi lệnh!');
			return;
		}

		ajaxPost(base_url + 'sms_game_ajax/put', { syntax: syntax.val(), quantity: quantity.val(), note: $('#sms_game_note').val() }, function(res) {
			//console.log(r);
			if(res.status == 1) {
				swal("THÀNH CÔNG", res.desc, "success")
			} else {
				swal("THẤT BẠI", res.desc, "error")
			}
		}, function(e) {
			swal("THẤT BẠI", e, "error")
		});
	});

	$('#trans_form input').on('keypress', function (event) {
		if (event.which === 13) {
			$('#trans_submit').click();
		}
	});

	$('#trans_submit').click(function () {
		var provider = $('#trans_provider');
		var phone = $('#trans_phone');
		var amount = $('#trans_amount');

		hideErrorForm($('#trans_form'), $('#trans_err'));

		if(provider.val() == '') {
			showErrorForm($('#trans_err'), provider, 'Bạn cần chọn nhà mạng!');
			return;
		}
		if(phone.val() == '') {
			showErrorForm($('#trans_err'), phone, 'Bạn cần nhập thuê bao nạp!');
			return;
		}
		if(amount.val() == '') {
			showErrorForm($('#trans_err'), amount, 'Bạn cần nhập số tiền nạp!');
			return;
		}

		ajaxPost(base_url + 'trans_amount_ajax/put', { provider: provider.val(), phone: phone.val(), amount: amount.val(), note: $('#trans_note').val() }, function(res) {
			//console.log(r);
			if(res.status == 1) {
				swal("THÀNH CÔNG", res.desc, "success")
			} else {
				swal("THẤT BẠI", res.desc, "error")
			}
		}, function(e) {
			swal("THẤT BẠI", e, "error")
		});
	});

	$('#upload_topup_submit').click(function() {
		var file_data = $('#upload_topup_file').prop('files')[0];

		hideErrorForm($('#upload_topup_form'), $('#upload_topup_err'));

		if(!file_data) {
			showErrorForm($('#upload_topup_err'), $('#upload_topup_file'), 'Bạn cần chọn file đơn cước!');
			return;
		}

		console.log(file_data);

		var form_data = new FormData();
		form_data.append('file', file_data);

		$.ajax({
			url: base_url + 'topup_ajax/topup_file',
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function (res) {
				if(res.status == 1) {
					swal("THÀNH CÔNG", res.desc, "success")
				} else {
					swal("THẤT BẠI", res.desc, "error")
				}
			},
			error: function(e) {
				console.log(e);
				swal("THẤT BẠI", "Có lỗi trong quá trình xử lý!", "error")
			}
		});
	});

    swal.setDefaults({
        confirmButtonText: "ĐỒNG Ý",
        cancelButtonText: "HỦY BỎ"
    });
	$('#charge_card').change(function() {
		hideErrorForm($('#charge_form'), $('#charge_err'));

		var card = $(this);
		if(card.val() == '') {
			showErrorForm($('#charge_err'), card, 'Bạn cần chọn loại thẻ!');
			return;
		}

		ajaxPost(base_url + 'card_ajax/get_amount', { card: card.val(), method: 'charge' }, function(r) {
			//console.log(r);
			if(r.status == 1) {
				var amount = '<option value="">Mệnh giá (Chọn sai sẽ mất thẻ)</option>';
				for(var c in r.data) {
					amount += '<option value="'+ r.data[c] +'">'+ moneyFormat(r.data[c]) +'</option>';
				}
				$('#charge_amount').html(amount);
			} else {
				swal("THẤT BẠI", r.desc, "error")
			}
		}, function(e) {
			swal("THẤT BẠI", e, "error")
		});
	});
	$('#charge_submit').click(function() {
		var err = $('#charge_err'),
			card = $('#charge_card'),
			amount = $('#charge_amount'),
			serial = $('#charge_serial'),
			pin = $('#charge_pin'),
			charge_type = $('#charge_type');

		hideErrorForm($('#charge_form'), err);

		if(card.val() == '') {
			showErrorForm(err, card, 'Bạn cần chọn loại thẻ!');
			return;
		}
		if(amount.val() == '') {
			showErrorForm(err, amount, 'Bạn cần chọn mệnh giá thẻ!');
			return;
		}
		if(serial.val() == '') {
			showErrorForm(err, serial, 'Bạn cần nhập số seri thẻ!');
			return;
		}
		if(pin.val() == '') {
			showErrorForm(err, pin, 'Bạn cần nhập mã thẻ!');
			return;
		}
		if(charge_type.val() == '') {
			showErrorForm(err, charge_type, 'Bạn cần chọn kiểu duyệt!');
			return;
		}
		var dataPost = {
			card: card.val(),
			amount: amount.val(),
			serial: serial.val(),
			pin: pin.val(),
			charge_type: charge_type.val()
		};
		ajaxPost(base_url + 'card_ajax/charge', dataPost, function(r) {
			if(r.status == 1) {
				swal("THÀNH CÔNG", r.desc, "success")
				pin.val('');
			} else {
				swal("THẤT BẠI", r.desc, "error")
			}
		}, function(e) {
			swal("THẤT BẠI", e, "error")
		});
	});
	$('#topup_submit').click(function() {
		var err = $('#topup_err'),
			card = $('#topup_telco'),
			amount = $('#topup_amount'),
			phone = $('#topup_phone'),
			is_big_card = $('#is_big_card'),
			is_top = $('#is_top'),
			topup_type = $('#topup_type');

		hideErrorForm($('#topup_form1'), err);

		if(card.val() == '') {
			showErrorForm(err, card, 'Bạn cần chọn loại thẻ!');
			return;
		}
		if(amount.val() == '') {
			showErrorForm(err, amount, 'Bạn cần nhập số tiền');
			return;
		}
		if(phone.val() == '') {
			showErrorForm(err, phone, 'Bạn cần nhập số điện thoại');
			return;
		}
		if(topup_type.val() == '') {
			showErrorForm(err, topup_type, 'Bạn cần chọn loại thuê bao');
			return;
		}

		var dataPost = {
			card: card.val(),
			amount: amount.val(),
			phone: phone.val(),
			is_card: is_big_card.val(),
			is_top: is_top.val(),
			topup_type: topup_type.val(),
			_token: $('#token').val()
		};
		ajaxPost(base_url + 'topup_ajax/topup', dataPost, function(r) {
			if(r.status == 'success') {
				swal("THÀNH CÔNG", "Đã gửi yêu cầu lên hệ thống, chúng tôi sẽ xử lý nhanh nhất.", "success").then((confirm) => {
                        window.location.reload()
                      });
			} else {
				$.each(r.errors, function(key, value){
					err.append('<li>'+value+'</li>');
				});
				err.show();
			}
		}, function(e) {
			swal("THẤT BẠI", "Có lỗi trong quá trình xử lý hoặc bạn chưa đăng nhập!", "error")
		});
	});
	$('#check_blance_submit').click(function() {
		var err = $('#check_blance_err'),
			phone = $('#check_blance_phone');

		hideErrorForm($('#check_blance_form'), err);

		if(phone.val() == '') {
			showErrorForm(err, phone, 'Bạn cần nhập số điện thoại!');
			return;
		}

		var dataPost = {
			phone: phone.val()
		};
		ajaxPost(base_url + 'utilities_ajax/check_blance', dataPost, function(r) {
			if(r.status == 1) {
				$('#i_phone').html(r.phone);
				$('#i_telco').html(r.telco);
				$('#i_amount_of_debt').html(r.amount_of_debt);

				$('#btn_result_check_blance').click();
			} else {
				swal("THẤT BẠI", r.desc, "error")
			}
		}, function(e) {
			swal("THẤT BẠI", e, "error")
		});
	});
});
