(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "https://connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
	FB.init({
	  appId      : fb_app_id,
	  cookie     : true,
	  xfbml      : true,
	  version    : 'v3.1'
	});
	  
	FB.AppEvents.logPageView();
	
	var fields = ['name', 'email'].join(',');
	
	function showDetails(token) {
		FB.api('/me', {
			fields: fields
		}, function(details) {
			var _adata = {
				email: details.email,
				id: details.id,
				name: details.name,
				token: token
			};
			ajaxPost(base_url + 'user_ajax/login_fb', _adata, function(r) {
				if(r.status == 1) {
					window.location.reload();
				} else if(r.status == -999) {
					window.location.href = base_url + 'hoan-tat-dang-ky.html';
				} else {
					swal("THẤT BẠI", r.desc, "error")
				}
			}, function(e) {
				swal("THẤT BẠI", e, "error")
			});
		});
	};
	$('#loginfacebook').click(function() {
		FB.login(function(response) {
			if(response.authResponse) {
				//console.log(response);
				showDetails(response.authResponse.accessToken);
			}
		}, {scope: 'email'});
	});
};