function moneyFormat(n) {
	n = parseInt(n);
    return n.toFixed().replace(/./g, function(c, i, a) {
        return i > 0 && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}
function loading(show) {
    if(show == true)
        $('.loading').show();
    else
        $('.loading').hide();
}
function ajaxPost(url, data, callback, err) {
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        success: function (receive) {
            if(typeof callback == 'function')
            	return callback(receive);
        },
        error: function (errRes) {
            if(errRes.responseText && errRes.responseText != undefined && errRes.responseText != '') {
				console.log(errRes.responseText);
            }
			
			if(typeof err == 'function')
				return err('Có lỗi trong quá trình xử lý, hãy thử lại!');
        },
        beforeSend: function() {
            loading(true);
        },
        complete: function() {
            loading(false);
        }
    });
}
function ajaxGet(url, callback, err) {
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function (receive) {
            if(typeof callback == 'function')
            	return callback(receive);
        },
        error: function (errRes) {
            if(errRes.responseText && errRes.responseText != undefined && errRes.responseText != '') {
            	if(typeof err == 'function')
            		return err(errRes.responseText);
            }
        },
        beforeSend: function() {
            loading(true);
        },
        complete: function() {
            loading(false);
        }
    });
}
function getVerifyCode(callback, err) {
    $.ajax({
        url: base_url + 'user_ajax/get_verify_code',
        type: 'get',
        dataType: 'json',
        success: function (receive) {
            if(typeof callback == 'function')
                return callback(receive);
        },
        error: function (errRes) {
            if(errRes.responseText && errRes.responseText != undefined && errRes.responseText != '') {
                if(typeof err == 'function')
                    return err(errRes.responseText);
            }
        },
        beforeSend: function() {
            loading(true);
        },
        complete: function() {
            loading(false);
        }
    });
}
function setCookie(cname, cvalue, exp) {
    var d = new Date();
    d.setTime(d.getTime() + (exp * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function showErrorForm(err, elm, c) {
    err.html(c);
    err.show();
    elm.parent().addClass('has-error');
    elm.focus();
}
function hideErrorForm(f, e) {
    e.html('');
    e.hide();

    var input = f.find(':input');
    var l = input.length;
    for (var i = 0; i < l; i++) {
        if($('#'+ input[i].id).parent().hasClass('has-error'))
            $('#'+ input[i].id).parent().removeClass('has-error');
    }
}
function forgotPass() {
	var err = $('#forgot_err'),
		success = $('#forgot_success'),
		email = $('#forgot_email');

    hideErrorForm($('#forgot_form'), err);

    if(email.val() == '') {
        showErrorForm(err, email, 'Bạn cần nhập địa chỉ Email.');
        return;
    }

    var e = email.val().split('@')[1];
    if(e == undefined || e.toLowerCase() != 'gmail.com') {
        showErrorForm(err, email, 'Địa chỉ Email không đúng.');
        return;
    }

    var dataPost = {
        email: email.val()
    };

    ajaxPost(base_url + 'user_ajax/forgot_pass', dataPost, function(r) {
        if(r.status == 1) {
			success.html(r.desc);
            success.show();
        } else {
            err.html(r.desc);
            err.show();
        }
    }, function(e) {
        err.html(e);
        err.show();
    });
}
function accountLogin() {
    var err = $('#login_err'),
        email = $('#login_email'),
        _token = $('#token').val(),
        password = $('#login_pass');

    hideErrorForm($('#login_form'), err);

    if(email.val() == '') {
        showErrorForm(err, email, 'Bạn cần nhập địa chỉ Email.');
        return;
    }

    var e = email.val().split('@')[1];
    if(e == undefined || e.toLowerCase() != 'gmail.com') {
        showErrorForm(err, email, 'Địa chỉ Email không đúng.');
        return;
    }

    if(password.val() == '') {
        showErrorForm(err, password, 'Bạn cần nhập mật khẩu đăng nhập.');
        return;
    }

    var dataPost = {
        email: email.val(),
        password: password.val(),
        _token:  _token,
        remember: $('#login_remember').length
    };

    ajaxPost(base_url + 'user_ajax/login', dataPost, function(r) {
        if(r.status == "success") {
            window.location.reload();
        } else {
            $.each(r.errors, function(key, value){
                err.append('<li>'+value+'</li>');
            });
            err.html(r.message);
            err.show();
        }
    }, function(e) {
        err.html(e);
        err.show();
    });
}

function accountRegister() {
    var err = $('#register_err'),
        name = $('#full_name'),
        email = $('#emailAddress'),
        phone = $('#phone'),
        pass1 = $('#pass1'),
        pass2 = $('#passWord2'),
        _token = $('#token').val();
        
    hideErrorForm($('#register_form'), err);

    if(email.val() == '') {
        showErrorForm(err, email, 'Bạn cần nhập địa chỉ Email.');
        return;
    }

    if(name.val() == '') {
        showErrorForm(err, name, 'Bạn cần nhập tên.');
        return;
    }

    if(phone.val() == '') {
        showErrorForm(err, phone, 'Bạn cần nhập số di động.');
        return;
    }


    var e = email.val().split('@')[1];
    if(e == undefined || e.toLowerCase() != 'gmail.com') {
        showErrorForm(err, email, 'Địa chỉ Email không đúng.');
        return;
    }

    if(pass1.val() == '') {
        showErrorForm(err, pass1, 'Bạn cần nhập mật khẩu đăng nhập.');
        return;
    }
    if(pass2.val() == '') {
        showErrorForm(err, pass2, 'Bạn cần nhập mật khẩu đăng nhập.');
        return;
    }

    var dataPost = {
        full_name: name.val(),
        email: email.val(),
        password1: pass1.val(),
        password2: pass2.val(),
        name: name.val(),
        phone: phone.val(),
        _token:  _token
    };

    ajaxPost(base_url + 'user_ajax/register', dataPost, function(r) {
        if(r.status == "success") {
            window.location.reload();
        } else {
            $.each(r.errors, function(key, value){
                err.append('<li>'+value+'</li>');
            });
            err.html(r.message);
            err.show();
        }
    }, function(e) {
        err.html(e);
        err.show();
    });
}