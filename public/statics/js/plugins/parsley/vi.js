Parsley.addMessages('vi', {
  defaultMessage: "Giá trị nhập vào không hợp lệ.",
  type: {
    email:        "Bạn cần nhập Email hợp lệ.",
    url:          "Bạn cần nhập URL hợp lệ.",
    number:       "Bạn chỉ được nhập số.",
    integer:      "Bạn chỉ được nhập số nguyên.",
    digits:       "Bạn chỉ được nhập chữ số.",
    alphanum:     "Bạn cần nhập cả số và chữ."
  },
  notblank:       "Trường này không được để trống.",
  required:       "Bạn cần nhập trường này.",
  pattern:        "Giá trị này có vẻ không hợp lệ.",
  min:            "This value should be greater than or equal to %s.",
  max:            "This value should be lower than or equal to %s.",
  range:          "This value should be between %s and %s.",
  minlength:      "Quá ngắn, bạn cần nhập %s ký tự trở lên.",
  maxlength:      "This value is too long. It should have %s characters or fewer.",
  length:         "This value length is invalid. It should be between %s and %s characters long.",
  mincheck:       "You must select at least %s choices.",
  maxcheck:       "You must select %s choices or fewer.",
  check:          "You must select between %s and %s choices.",
  equalto:        "Xác nhận mật khẩu không đúng."
});

Parsley.setLocale('vi');