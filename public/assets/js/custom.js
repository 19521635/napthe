$(function() {
    $('.edit-user').click(function () {
        var Id_user = $(this).val();
        $.ajax({
            url: 'admin_ajax/getJsonUsers',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id: Id_user,
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                var res = data;
                $('#elegantModalForm').find('#id').val(res.id);
                if (res.is_admin==1) {
                    $('#elegantModalForm').find('#role-admin').attr('selected','selected');
                } else {
                    $('#elegantModalForm').find('#role-user').attr('selected','selected');      
                }
                $('#elegantModalForm').find('#name').val(res.name);
                $('#elegantModalForm').find('#email').val(res.email);
                $('#elegantModalForm').find('#phone').val(res.phonenumber);
                $('#elegantModalForm').modal('show');
            },
            error:function(data) {
                swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
            }
        });
    });
    $('.edit-otp').click(function () {
        var Id_phone = $(this).val();
        $.ajax({
            url: 'admin_ajax/getJsonOtps',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                phone: Id_phone,
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                var res = data;
                $('#elegantModalForm').find('#phone').val(res.phonenumber);
                $('#elegantModalForm').find('#otp').val(res.otp);
                $('#elegantModalForm').find('#otp_password').val(res.otp_password);
                $('#elegantModalForm').modal('show');
            },
            error:function(data) {
                swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
            }
        });
    });
    $('.edit-card').click(function () {
        var Id_card = $(this).val();
        $.ajax({
            url: 'admin_ajax/updateCard',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id: Id_card,
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                if (data.status=='success') {
                    swal("Hoàn tất!", "Đã thay đổi trạng thái!", "success").then((confirm) => {
                        window.location.reload()
                      });
                } else {
                    swal("Lỗi!", "Có lỗi xảy ra!", "error")
                }
            },
            error:function(data) {
                swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
            }
        });
    });
    $('#updateUser').click(function () {
        $.ajax({
            url: 'admin_ajax/updateUser',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id: $('#elegantModalForm').find('#id').val(),
                name: $('#elegantModalForm').find('#name').val(),
                email: $('#elegantModalForm').find('#email').val(),
                phone: $('#elegantModalForm').find('#phone').val(),
                password: $('#elegantModalForm').find('#password').val(),
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                if (data.status=='success') {
                    swal("Hoàn tất!", "Update thành công!", "success").then((confirm) => {
                        window.location.reload()
                      });
                } else if (data.status=='errors') {
                    swal("Lỗi!", "Email đã có người sử dụng!", "error").then((confirm) => {
                        window.location.reload()
                      });
                } else {
                    swal("Lỗi!", data.error[0] , "error").then((confirm) => {
                        window.location.reload()
                      });
                }
            },
            error:function(data) {
                swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
            }
        });
    });
    $('#updateOtp').click(function () {
        $.ajax({
            url: 'admin_ajax/updateOtp',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                phone: $('#elegantModalForm').find('#phone').val(),
                otp: $('#elegantModalForm').find('#otp').val(),
                otp_password: $('#elegantModalForm').find('#otp_password').val(),
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                if (data.status=='success') {
                    swal("Hoàn tất!", "Update thành công!", "success").then((confirm) => {
                        window.location.reload()
                      });
                } else {
                    swal("Lỗi!", "Có lỗi xảy ra!", "error")
                }
            },
            error:function(data) {
                swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
            }
        });
    });
    $('.delete-user').click(function () {
        swal({
            title: "Bạn có chắc?",
            text: "Sẽ không thể khôi phục đuợc user!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: false, 
            buttons: true,
            dangerMode: true
          }).then((isConfirm) => {
            if (isConfirm) {
                var Id_user = $(this).val();
                $.ajax({
                    url: 'admin_ajax/deleteUser',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        id: Id_user,
                        _token: $('#token').val()
                    },
                    dataType : 'json',
                    success: function(data){ 
                        if (data.status=='success') {
                            swal("Hoàn tất!", "User đã được xoá!", "success").then((confirm) => {
                                window.location.reload()
                              });
                        } else {
                            swal("Lỗi!", "Có lỗi xảy ra!", "error")
                        }
                    },
                    error:function(data) {
                        swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
                    }
                });
            } else {
              swal("Hủy bỏ", "Đã hủy thao tác!", "error");
            }
          });
        
    });
    $('.delete-card').click(function () {
        swal({
            title: "Bạn có chắc?",
            text: "Sẽ không thể khôi phục đuợc card!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: false, 
            buttons: true,
            dangerMode: true
          }).then((isConfirm) => {
            if (isConfirm) {
                var Id_card = $(this).val();
                $.ajax({
                    url: 'admin_ajax/deleteCard',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        id: Id_card,
                        _token: $('#token').val()
                    },
                    dataType : 'json',
                    success: function(data){ 
                        if (data.status=='success') {
                            swal("Hoàn tất!", "Card đã được xoá!", "success").then((confirm) => {
                                window.location.reload()
                              });
                        } else {
                            swal("Lỗi!", "Có lỗi xảy ra!", "error")
                        }
                    },
                    error:function(data) {
                        swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
                    }
                });
            } else {
              swal("Hủy bỏ", "Đã hủy thao tác!", "error");
            }
          });
    });

    $('.delete-otp').click(function () {
        swal({
            title: "Bạn có chắc?",
            text: "Sẽ không thể khôi phục đuợc otp!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: false, 
            buttons: true,
            dangerMode: true
          }).then((isConfirm) => {
            if (isConfirm) {
                var Id_phone = $(this).val();
                $.ajax({
                    url: 'admin_ajax/deleteOtp',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        phone: Id_phone,
                        _token: $('#token').val()
                    },
                    dataType : 'json',
                    success: function(data){ 
                        if (data.status=='success') {
                            swal("Hoàn tất!", "otp đã được xoá!", "success").then((confirm) => {
                                window.location.reload()
                              });
                        } else {
                            swal("Lỗi!", "Có lỗi xảy ra!", "error")
                        }
                    },
                    error:function(data) {
                        swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
                    }
                });
            } else {
              swal("Hủy bỏ", "Đã hủy thao tác!", "error");
            }
          });
        
    });

    $('.cash-user').click(function () {
        var Id_user = $(this).val();
        $.ajax({
            url: 'admin_ajax/getCashUser',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id: Id_user,
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                var res = data;
                $('#elegantModalForm1').find('#id').val(res.id);
                $('#elegantModalForm1').find('#cash').val(res.cash);
                $('#elegantModalForm1').modal('show');
            },
            error:function(data) {
                swal("Lỗi", "Lỗi khi gửi dữ liệu!", "error");
            }
        });
        
    });
    $('#updateCash').click(function () {
        $.ajax({
            url: 'admin_ajax/updateCash',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                cash: $('#elegantModalForm1').find('#cash').val(),
                id: $('#elegantModalForm1').find('#id').val(),
                _token: $('#token').val()
            },
            dataType : 'json',
            success: function(data){ 
                if (data.status=='success') {
                    swal("Hoàn tất!", "Update thành công!", "success").then((confirm) => {
                        window.location.reload()
                      });
                } else {
                    swal("Lỗi!", "Có lỗi xảy ra!", "error")
                }
            },
            error:function(data) {
                swal("Lỗi!", "Có lỗi khi gửi dữ liệu!", "error")
            }
        });
    });
});
